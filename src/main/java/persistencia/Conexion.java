/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package persistencia;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Producto;

/**
 *
 * @author ingdeiver
 */
public class Conexion {
    
    private String URL = "reto5.db";
    private PreparedStatement stm = null;
    private ResultSet rs  = null;
    private Connection connection = null;
    
    public Conexion(){
        try {
            // nos conectamos
            connection = DriverManager.getConnection("jdbc:sqlite:"+URL);
            if(this.connection != null){
                System.out.println("Conexion con base de datos exitosa");
            }
        } catch (Exception e) {
            System.out.println("Error al conectarse: " + e.getMessage());
        }
    }
    
    
    public List<Producto> listarProductos(){
        List<Producto> productos = new ArrayList<>();
        try {
            // ejecutar el stm
            stm = this.connection.prepareStatement("SELECT * FROM productos;");
            rs = stm.executeQuery();
            // convierto los registros de la db a objetos en java
            while(rs.next()){
                Producto producto = new Producto();
                int codigo = rs.getInt("id");
                String  nombre = rs.getString("nombre");
                double precio = rs.getDouble("precio");
                
                // La info de la db la seteo en el objeto de Java
                producto.setCodigo(codigo);
                producto.setNombre(nombre);
                producto.setPrecio(precio);
                
                productos.add(producto);
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
        return productos;
    }
}
